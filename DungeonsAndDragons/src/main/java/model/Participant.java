package model;

import java.sql.Date;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Participant {
	@NotNull
	private String name;
	@NotNull
	@Min(value=1)
	private int id;
	@Past
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date birthday;
	private String gender;
	private boolean gameMaster;
	private PlayerCharacter pc;

	
	public Participant() {
		
		
	}
	
	public Participant(String name, int id, Date birthday, String gender, boolean gameMaster) {
		super();
		this.name = name;
		this.id = id;
		this.birthday = birthday;
		this.gender = gender;
		this.gameMaster = gameMaster;
	}
	
	public Participant(String name, int id, Date birthday, String gender, boolean gameMaster, PlayerCharacter pc) {
		super();
		this.name = name;
		this.id = id;
		this.birthday = birthday;
		this.gender = gender;
		this.gameMaster = gameMaster;
		this.pc = pc;
	}
	
	
	
	public int calcAge() {
		//TODO
		return 18;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public PlayerCharacter getPc() {
		return pc;
	}

	public void setPc(PlayerCharacter pc) {
		this.pc = pc;
	}
	
	public boolean isGameMaster() {
		return gameMaster;
	}

	public void setGameMaster(boolean gameMaster) {
		this.gameMaster = gameMaster;
	}
	
	
	public boolean isValidDate(Date date) {
		return LocalDate                                      // Represents a date-only, without time-of-day and without time zone.
		.parse( 
		     date.toString() , 
		    DateTimeFormatter.ofPattern( "yyyy-MM-dd")  // Specify a format to match you're input.
		)                                              // Returns a `LocalDate` object.
		.isBefore(                                     // Compare one `LocalDate` to another.
		    LocalDate.now(                             // Capture the current date…
		    )                                          // Returns a `LocalDate` object.
		);   
	}


	
	

}
