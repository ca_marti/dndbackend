package model;

import java.util.ArrayList;


public class Campaign {
	
	private String name;
	private int id;
	private boolean isActiv;
	
	private Participant gameMaster;
	private ArrayList<Participant> players = new ArrayList<Participant>();
	
	
	public Campaign() {
		
	}
	
	public Campaign(String name, int id, boolean isActiv, Participant gameMaster) {
		this.name = name;
		this.id = id;
		this.isActiv = isActiv;
		this.gameMaster = gameMaster;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isActiv() {
		return isActiv;
	}
	public void setActiv(boolean isActiv) {
		this.isActiv = isActiv;
	}
	public Participant getGameMaster() {
		return gameMaster;
	}
	public void setGameMaster(Participant gameMaster) {
		this.gameMaster = gameMaster;
	}
	public void addPlayer(Participant p) {
		this.players.add(p);
	}
	 
	
}
