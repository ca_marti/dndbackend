package model;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

public class PlayerCharacter {
	@NotNull
	private String name;
	@NotNull
	@Min(value=1)
	private int id;
	private String characterClass;
	private String subclass;
	@Min(value=1)
	@Max(value=20)
	private int level;
	private String race;
	@Min(value=10)
	private int armorClass;

	
	
	public PlayerCharacter() {
		
	}
	
	public PlayerCharacter(String name, int id, String characterClass, String subclass, int level, String race,
			int armorClass) {
		super();
		this.name = name;
		this.id = id;
		this.characterClass = characterClass;
		this.subclass = subclass;
		this.level = level;
		this.race = race;
		this.armorClass = armorClass;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCharacterClass() {
		return characterClass;
	}

	public void setCharacterClass(String characterClass) {
		this.characterClass = characterClass;
	}

	public String getSubclass() {
		return subclass;
	}

	public void setSubclass(String subclass) {
		this.subclass = subclass;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public int getArmorClass() {
		return armorClass;
	}

	public void setArmorClass(int armorClass) {
		this.armorClass = armorClass;
	}
	
	

}
