package dao;

import java.sql.Connection;
import java.sql.SQLException;

public interface IDAO<T> {
	public T selectById(int id) throws SQLException;
	public int insert(T r);
	public int update(T r);
	public int delete(int id);
	public Connection connectMe(String dbname) throws SQLException;
	
}
