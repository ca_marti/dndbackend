package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Participant;

public class ParticipantDAO implements IDAO<Participant>{

	@Override
	public Participant selectById(int id) throws SQLException {
		Participant p = new Participant();
		Connection con = connectMe("dndDB");
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("Select idParticipant, name, birthday, gender,  isGameMaster, "
				+ "idCampaign, idCharacter FROM participant where idParticipant = " + id + ";");
		
		if(rs.next()) {
			p.setId(rs.getInt(1));
			p.setName(rs.getString(2));
			p.setBirthday(rs.getDate(3));
			p.setGender(rs.getString(4));
			p.setGameMaster(rs.getBoolean(5));
			//TODO potentially finding game and game and character in db
			
			return p;
		} else {
			return null;
		}
	}
	
	public ArrayList<Participant> selectAll() throws SQLException{
		
		ArrayList<Participant> pl = new ArrayList<Participant>();
		Connection con = connectMe("dndDB");
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("Select idParticipant, name, birthday, gender,  isGameMaster, "
				+ "idCampaign, idCharacter FROM participant;");
		
		while(rs.next()) {
			pl.add(new Participant(rs.getString(2), rs.getInt(1), rs.getDate(3),
					rs.getString(4), rs.getBoolean(5)));
		}
		
		return pl;
		
	}
	
	public ArrayList<Participant> selectByName(String name) throws SQLException {
		ArrayList<Participant> pl = new ArrayList<Participant>();
		Connection con = connectMe("dndDB");
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("Select idParticipant, name, birthday, gender,  isGameMaster, "
				+ "idCampaign, idCharacter FROM participant WHERE name='"+ name +"';");
		
		while(rs.next()) {
			pl.add(new Participant(rs.getString(2), rs.getInt(1), rs.getDate(3),
					rs.getString(4), rs.getBoolean(5)));
		}
		
		return pl;
	}


	@Override
	public int insert(Participant p) {
		try {
			Connection con = connectMe("dnddb");
			Statement st = con.createStatement();
			int rs = st.executeUpdate("INSERT INTO participant (idParticipant, name, birthday, gender,  isGameMaster)"
					+ "VALUES ('" + p.getId() + "', '" + p.getName() + "', '" + p.getBirthday() + "', '" + p.getGender()
					+ "', " + true + ");");
			return rs;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	

	@Override
	public int update(Participant p) {
		return 1;
	}

	@Override
	public int delete(int id) {
		
		return 1;
	}
	
	
	
	

	@Override
	public Connection connectMe(String dbname) throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/" + dbname, "admin_carmen", "Mensh1999");
	}

	
}
