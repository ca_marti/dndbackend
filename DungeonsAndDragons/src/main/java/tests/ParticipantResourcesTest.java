package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.jupiter.api.Test;

import jakarta.ws.rs.core.HttpHeaders;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;





class ParticipantResourcesTest {

private static final String URL = "http://localhost:8080/DungeonsAndDragons/resources/"; 
	
	@Test
	void getAllAccessDenied() throws ClientProtocolException, IOException {
		HttpUriRequest req = new HttpGet(URL + "participant/findAll");
		
		CloseableHttpResponse resp = HttpClientBuilder
				.create()
				.build()
				.execute(req);
		
		assertEquals(HttpStatus.SC_FORBIDDEN, resp
		          .getStatusLine()
		          .getStatusCode());
	}
	
	
	@Test
	void getByIdSuccess() throws ClientProtocolException, IOException {
		HttpUriRequest req = new HttpGet(URL + "participant/findParticipant/1");
		
		CloseableHttpResponse resp = HttpClientBuilder
				.create()
				.build()
				.execute(req);
		
		assertEquals(HttpStatus.SC_OK, resp
		          .getStatusLine()
		          .getStatusCode());
	}
	
	
	
	@Test
	void insertBadBirthday() throws ClientProtocolException, IOException {
		HttpPost req = new HttpPost (URL + "participant/insert2");
		
		String auth = "admin" + ":" + "1234";
        byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.ISO_8859_1));
        String authHeader = "Basic " + new String(encodedAuth);
        req.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
        
        String playerJson="{\r\n"
                + "    \"id\": 65,\r\n"
                + "    \"name\": \"Suada\",\r\n"
                + "    \"gender\": \"Female\",\r\n"
                + "    \"GameMaster\": 0,\r\n"
                + "    \"birthday\": \"2027-04-23\",\r\n"
                + "    \"pc\": \"null\"\r\n"
                + "  }";
        
        req.setEntity(new StringEntity(playerJson,ContentType.APPLICATION_JSON));
        
        CloseableHttpResponse httpResponse = (CloseableHttpResponse) HttpClients.createDefault().execute(req
                , response -> {
                    EntityUtils.consume(response.getEntity());
                    return response;
                }
                );
        assertEquals(HttpStatus.SC_BAD_REQUEST, httpResponse
                  .getStatusLine()
                  .getStatusCode());
	}
	
	@Test
	void insertSuccess() throws ClientProtocolException, IOException {
		HttpPost req = new HttpPost (URL + "participant/insert2");
		
		String auth = "admin" + ":" + "1234";
        byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.ISO_8859_1));
        String authHeader = "Basic " + new String(encodedAuth);
        req.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
        
        
        
        String playerJson="{"
                + "\"id\": 160,"
                + "\"name\": \"Suada\","
                + "\"gender\": \"Female\","
                + "\"gameMaster\": 0,"
                + "\"birthday\": \"2023-12-19\""
                + "}";
        
        req.setEntity(new StringEntity(playerJson,ContentType.APPLICATION_JSON));
        
        CloseableHttpResponse rep = (CloseableHttpResponse) HttpClients.createDefault().execute(req
                , response -> {
                    EntityUtils.consume(response.getEntity());
                    return response;
                }
                );
        assertEquals(HttpStatus.SC_OK, rep
                  .getStatusLine()
                  .getStatusCode());
	}
	
	
	
	@Test
	public void deleteDenied() throws ClientProtocolException, IOException {
		HttpDelete request = new HttpDelete(URL + "participant/delete");
		
		String auth = "admin" + ":" + "1234";
    	byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.ISO_8859_1));
    	String authHeader = "Basic " + new String(encodedAuth);
    	request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
		
		CloseableHttpResponse httpResponse = (CloseableHttpResponse) HttpClients.createDefault().execute(request,
				response -> {
					System.out.println("----------------------------------------");
					System.out.println(request + "->" + response.getStatusLine());
					EntityUtils.consume(response.getEntity());
					return response;
 
				}
 
		);
		assertEquals(HttpStatus.SC_FORBIDDEN, httpResponse
  	          .getStatusLine()
  	          .getStatusCode());
	}
	
	
	@Test
    public void findByName()
      throws ClientProtocolException, IOException {
 
        HttpUriRequest request = new HttpGet(URL + "participant/findParticipantByName/Luca");
        
        String auth = "admin" + ":" + "1234";
    	byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.ISO_8859_1));
    	String authHeader = "Basic " + new String(encodedAuth);
    	request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
 
        CloseableHttpResponse httpResponse = HttpClientBuilder
          .create()
          .build()
          .execute(request);
 
        assertEquals(HttpStatus.SC_OK, httpResponse
          .getStatusLine()
          .getStatusCode());
    }
	
	
	@Test
    public void UpdateSucess()
      throws ClientProtocolException, IOException {
 
        HttpUriRequest request = new HttpGet(URL + "participant/findParticipantByName/Luca");
        
        String auth = "admin" + ":" + "1234";
    	byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.ISO_8859_1));
    	String authHeader = "Basic " + new String(encodedAuth);
    	request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
 
        CloseableHttpResponse httpResponse = HttpClientBuilder
          .create()
          .build()
          .execute(request);
 
        assertEquals(HttpStatus.SC_OK, httpResponse
          .getStatusLine()
          .getStatusCode());
    }
	
	
	@Test
    public void UpdateFail()
      throws ClientProtocolException, IOException {
 
        HttpUriRequest request = new HttpGet(URL + "participant/findParticipantByName/ABCEFGHIJ");
        
        String auth = "admin" + ":" + "1234";
    	byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.ISO_8859_1));
    	String authHeader = "Basic " + new String(encodedAuth);
    	request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
 
        CloseableHttpResponse httpResponse = HttpClientBuilder
          .create()
          .build()
          .execute(request);
 
        assertEquals(HttpStatus.SC_BAD_REQUEST, httpResponse
          .getStatusLine()
          .getStatusCode());
    }
	
	
	
	
	

}
