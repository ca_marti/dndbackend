package services;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dao.PlayerCharacterDao;
import jakarta.annotation.security.DenyAll;
import jakarta.annotation.security.PermitAll;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.Response.Status;
import model.PlayerCharacter;

@Path("/chararcter")
public class PlayerCharacterResources {
	
	PlayerCharacterDao pcd = new PlayerCharacterDao();
	private PlayerCharacter pc;
	private Logger logger = LogManager.getLogger(ParticipantResources.class);
	
	@PermitAll
	@GET
	@Path("/findPC/{nr}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("nr") Integer id) {
		pc = null;
		
		logger.info("Found PCBYID");
		pc = pcd.selectById(id);
		
		if(!Objects.isNull(pc)) {
			return Response.status(Status.OK).entity(pc).build();
		}
		else {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
	}
	
	@DenyAll
	@GET
	@Path("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() {
		logger.info("IN: findAllParticipants (shouldn't happen because DenyAll)");
		ArrayList<PlayerCharacter> pcs = new ArrayList<PlayerCharacter>();
		pcs = pcd.selectAll();
		
		if(pcs.size() > 0) {
			return Response.status(Status.OK).entity(pc).build();
			
		}
		else {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
	}
	
	
	
	//Should return all Player Characters that have >= the given armorClass
	@RolesAllowed("ADMIN")
	@GET
	@Path("/findByMinAc/{ac}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findParticipantById(@PathParam("ac") int ac) {
		ArrayList<PlayerCharacter> pcs = new ArrayList<PlayerCharacter>();
		
		logger.info("IN: findParticipantByName");
		pcs = pcd.selectByMinAc(ac);
		
		if(pcs.size() > 0) {
			return Response.status(Status.OK).entity(pcs).build();
		}
		else {
			return Response.status(Status.BAD_REQUEST).build();
		}
	}
	
	
	
	
	
	@PermitAll	
	@POST
	@Path("/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertJson(@Valid PlayerCharacter pc) {
    	
    	logger.info("IN: insert");
		int anz = pcd.insert(pc);
		
		if(anz > 0) {
			return Response.ok(pc).build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

	}
	
	@RolesAllowed("ADMIN")
	@DELETE
	@Path("/delete")
	@Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response deleteById(@FormParam("pcid") int id) {
    	int anz = pcd.delete(id);
    	
    	if(anz > 0) {
    		return Response.ok(anz).build();
    	} else {
    		logger.info("Couldn't delete anything :(");
    		return Response.status(Response.Status.BAD_REQUEST).build();
    	}
		
	}
	
	
	@PermitAll
    @PUT
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateClassOfPlayer(PlayerCharacter pc) {
    	
    	logger.info("*Hacker voice:* I'm in Update");
    	ResponseBuilder rb;
    	int anz = pcd.update(pc);
    	
    	if(anz==1) {
			rb = Response.accepted("{\"Update\": \"ok\"}");
		}else {
			logger.info("Failed to update: " + pc.toString());
			rb = Response.status(393).entity("{\"Update\": \"failed\"}");
		}
		return rb.build();
    
    	
    }
	
	
	
	
	
	public PlayerCharacter getPc() {
		return pc;
	}
	public void setPc(PlayerCharacter pc) {
		this.pc = pc;
	}
	
	
	
	

}
