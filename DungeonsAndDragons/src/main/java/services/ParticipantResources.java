package services;

import java.sql.Date;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dao.ParticipantDAO;
import jakarta.annotation.security.DenyAll;
import jakarta.annotation.security.PermitAll;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.Response.Status;
import model.Participant;

@Path("/participant")
public class ParticipantResources {
	
	private ParticipantDAO pdao = new ParticipantDAO();
	private Participant participant;
	private Logger logger = LogManager.getLogger(ParticipantResources.class);
	
	@PermitAll
	@GET
	@Path("/findParticipant/{nr}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findParticipantById(@PathParam("nr") Integer id) {
		participant = null;
		logger.info("Found findParticipantById");
		try {
			participant = pdao.selectById(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.info("threw exception due to the pdao");
			e.printStackTrace();
		}
		
		if(!Objects.isNull(participant)) {
			return Response.status(Status.OK).entity(participant).build();
			
		}
		else {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
	}
	
		@PermitAll
		@GET
		@Path("/findParticipantByName/{name}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response findParticipantById(@PathParam("name") String name) {
			ArrayList<Participant> pl = new ArrayList<Participant>();
			
			logger.info("IN: findParticipantByName");
			try {
				pl = pdao.selectByName(name);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(pl.size() > 0) {
				return Response.status(Status.OK).entity(pl).build();
				
			}
			else {
				return Response.status(Status.BAD_REQUEST).build();
			}
			
		}
		
		//This must stay Deny all because we test for that
		@DenyAll
		@GET
		@Path("/findAll")
		@Produces(MediaType.APPLICATION_JSON)
		public Response findAll() {
			logger.info("IN: findAllParticipants (shouldn't happen because DenyAll");
			ArrayList<Participant> pl = new ArrayList<Participant>();
			try {
				 pl = pdao.selectAll();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			if(pl.size() > 0) {
				return Response.status(Status.OK).entity(pl).build();
				
			}
			else {
				return Response.status(Status.BAD_REQUEST).build();
			}
			
		}
	

	
	
    @RolesAllowed("ADMIN")
	@POST
	@Path("/insert")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insert(@Valid @FormParam("pID") Integer pID, 
			@Valid @FormParam("name") String name,
			@Valid @FormParam("birthday") Date bday, @FormParam("gender") String gender, 
			@FormParam("gameMaster") boolean gm) {
    	
    	logger.info("IN: insert (that means user is admin)");
    	
		participant = new Participant();
		participant.setId(pID);
		participant.setName(name);
		participant.setBirthday(bday);
		participant.setGender(gender);
		participant.setGameMaster(gm);
	
		int anz = pdao.insert(participant);
		
		if(anz > 0) {
			return Response.ok(participant).build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

	}
    
    @RolesAllowed("ADMIN")
   	@POST
   	@Path("/insert2")
   	@Consumes(MediaType.APPLICATION_JSON)
   	@Produces(MediaType.APPLICATION_JSON)
   	public Response insertJSON(@Valid Participant p) {
       	
       	logger.info("IN: insert (that means user is admin)");
       	ResponseBuilder rb;
   		int anz = pdao.insert(p);
   		
   		if(anz > 0) {
   			return Response.ok(p).build();
   		} else {
   			logger.info("Bad");
   			rb = Response.status(295).entity("{\"Insert\": \"unsucessful\"}");
   			return rb.build();
   		}

   	}
    
    
    
    @DenyAll
	@DELETE
	@Path("/delete")
	@Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response deleteById(@FormParam("pID") int id) {
    	int anz = pdao.delete(id);
    	
    	if(anz > 0) {
    		logger.info("Code should never go here because DenyAll");
    		return Response.ok(anz).build();
    	} else {
    		logger.info("Couldn't delete anything :(");
    		return Response.status(Response.Status.BAD_REQUEST).build();
    	}
		
	}
    
    @RolesAllowed("ADMIN")
    @PUT
    @Path("/updatePlayer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateClassOfPlayer(Participant p) {
    	
    	logger.info("*Hacker voice:* I'm in update Participant");
    	ResponseBuilder rb;
    	int anz = pdao.update(p);
    	
    	if(anz==1) {
			rb = Response.accepted("{\"ok\"}");
		}else {
			logger.info("Failed to update");
			rb = Response.status(295).entity("{\"Update\": \"failed\"}");
		}
		return rb.build();
    
    	
    }
    
	
	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	

}
