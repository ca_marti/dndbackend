package config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;
import security.AuthenticationFilter;
import services.ParticipantResources;
import services.PlayerCharacterResources;


@ApplicationPath("/resources")
public class RestConfig extends Application {
    public Set<Class<?>> getClasses() {
        return new HashSet<Class<?>>(Arrays.asList(ParticipantResources.class, AuthenticationFilter.class,
        		PlayerCharacterResources.class));
    }
}